public class PrivateShirt2Test {
	public static void main(String [] args){
		PrivateShirt2 privShirt = new PrivateShirt2();
		char kodeWarna;

		//mengisi kode warna yang valid
		privShirt.setKodeWarna('R');
		kodeWarna = privShirt.getKodeWarna();

		//kelas PrivateShirt2Test bisa mengisi sebuah kodeWarna yang valid
		System.out.println("Kode Warna : " +kodeWarna);
		
		//mengisi kode warna yang salah
		privShirt.setKodeWarna('Z');
		kodeWarna = privShirt.getKodeWarna();
		
		//kelas PrivateShirt2Test bisa mengisi sebuah kodeWarna yang salah 
		System.out.println("Kode Warna : "+kodeWarna);
	}
}	
