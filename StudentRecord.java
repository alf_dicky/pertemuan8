 public class StudentRecord{
	private String name;
	private String address;
	private int age;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
	private double average;
	private static int studentCount = 0;

	public StudentRecord(){

	}
	
	public StudentRecord(String temp){
		this.name = temp;
	}
	/**
	* Menghasilkan nama dari siswa
	*/
	public String getName(){
		return name;
	}

	/**
	* Mengubah nama siswa
	*/

	public void setName(String temp){
		studentCount++;
		name = temp;
	}
	//area penulisan kode lain

	/**
	* Menghitung rata-rata nilai Matematik, bahasa inggris, * * ilmu pasti
	*/

	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	
	/**
	* Menghasilkan jumlah instance StudentRecord
	*/

	public static int getStudentCount(){
		return studentCount;
	}

}
