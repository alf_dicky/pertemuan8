 public class PrivateShirt2 {
	private int idBaju = 0; // ID default untuk baju 
	private String keterangan ="-Keterangan Diperlukan=-"; //default

	//kode warna R=Merah, G=Hijau, B=Biru, U=Tidak ditentukan
	private char kodeWarna = 'U';
	private double harga = 0.0; //harga default untuk semua barang
	private int jmlStock = 0; //default  untuk jumlah barang 
	
	public char getKodeWarna() {
		return kodeWarna;
	}
	public void setKodeWarna(char kode){
		switch (kode) {
		case 'R' :
		case 'G' :
		case 'B' :
			kodeWarna = kode;
			break;
		default:
			System.out.println("Kode warna salah, gunakan R, G, atau B");
		}
	}	
	
	public int getIdBaju(){
		return idBaju;
	}
	public void setIdBaju(int idBaju){
		this.idBaju = idBaju;
	}
	public String getKeterangan(){
		return keterangan;
	}
	public void setKeterangan(String keterangan){
		this.keterangan = keterangan;
	}
	public double getHarga(){
		return harga;
	}
	public void setHarga(double harga){
		this.harga = harga;
	}
	public void setJmlStock(int jmlStock){
		this.jmlStock = jmlStock;
	}
	public int getJmlStock(){
		return jmlStock;
	}
}
