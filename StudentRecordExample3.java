public class StudentRecordExample3{
	public static void main(String [] args){
		//membuat 3 object StudentRecord
		StudentRecord3 annaRecord = new StudentRecord3();
		StudentRecord3 beahRecord = new StudentRecord3();
		StudentRecord3 crisRecord = new StudentRecord3();

		//versi baru yang ditambahkan
		StudentRecord3 karyono = new StudentRecord3("Karyono");
		StudentRecord3 songjongki = new StudentRecord3("Song Jong-Ki", "Cibaduyut");
		StudentRecord3 masbejo = new StudentRecord3(80, 90, 100);

		//Memberi nama siswa
		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");


		//Menampilkan nama siswa
		System.out.println(annaRecord.getName());
		System.out.println(beahRecord.getName());
		System.out.println(crisRecord.getName());
		

		//Menampilkan jumlah siswa
		System.out.println("Count = " +StudentRecord3.getStudentCount());
		StudentRecord3 anna2Record = new StudentRecord3();
		anna2Record.setName("Anna");
		anna2Record.setAddress("Philipina");
		anna2Record.setAge(15);
		anna2Record.setMathGrade(80);
		anna2Record.setEnglishGrade(95.5);
		anna2Record.setScienceGrade(100);
		
		

		//Overload method
		anna2Record.print(anna2Record.getName());
		anna2Record.print(anna2Record.getAddress());
		anna2Record.print(anna2Record.getEnglishGrade(), anna2Record.getMathGrade(), anna2Record.getScienceGrade());
		annaRecord.print(annaRecord.getName());
	}
}
